require('../src/strftime');

var should = require('should');

describe("stftime test", function() {
  var date;

  beforeEach(function() {
    date = new Date(2009, 9, 2, 22, 14, 45);
  });

  describe("test format specifier %Y", function() {
    it ("%Y should return full year", function() {
      Date.formats.Y(date).should.eql(2009);
    })
  })

  describe("test format specifier %m", function() {
    it ("%m should return month", function() {
      Date.formats.m(date).should.eql("10");
    })
  })

  describe("test format specifier %d", function() {
    it ("%d should return date", function() {
      Date.formats.d(date).should.eql("02");
    })
  })

  describe("test format specifier %y", function() {
    it ("%y should return year as two digits year", function() {
      Date.formats.y(date).should.eql("09");
    })
  })

  describe("test format shorthand %F", function() {
    it ("%F should be shortcut for %Y-%m-%d", function() {
      Date.formats.F.should.eql("%Y-%m-%d");
    })
  })

  describe("test format specifier %j", function() {
    it ("%j should return days from the first day of the year", function() {
      Date.formats.j(date).should.eql(275);
    })
  })

});